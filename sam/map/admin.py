from django.contrib import admin
from map.models import Site, Measurement

# Register your models here.
class SiteAdmin(admin.ModelAdmin):
	list_display = ('name', 'lat', 'lng')


class MeasurementAmdin(admin.ModelAdmin):
	list_display = ('site', 'timestamp')
	list_filter = ('site',)


admin.site.register(Site, SiteAdmin)
admin.site.register(Measurement, MeasurementAmdin)
