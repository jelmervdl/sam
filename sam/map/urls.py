from django.conf.urls import include, patterns, url
from django.contrib import admin
import views

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^data/sites/(\d+)$', views.data_site, name='map_data_site'),
	url(r'^data/(\d{4})/(\d{1,2})/(\d{1,2})$', views.data_day, name='map_data'),
	url(r'^test$', views.test, name='test'),
	url(r'^$', views.map, name='map'),
)
