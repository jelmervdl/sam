from django.db import models

# Create your models here.

class Site(models.Model):
	name = models.CharField(max_length=100)
	lat = models.FloatField()
	lng = models.FloatField()

	def __unicode__(self):
		return self.name


class Measurement(models.Model):
	site = models.ForeignKey('Site', related_name='measurements')
	timestamp = models.DateTimeField()
	pleasantness = models.FloatField()
	eventfulness = models.FloatField()

	def __unicode__(self):
		return u"%s at %s" % (self.site.name, self.timestamp)
