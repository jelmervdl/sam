function drawRubenCircle(ctx, x, y, outer, pleasantness, eventfulness)
{
	var lineWidth = outer / 5;
	var inner = (outer - lineWidth * 1.5) * eventfulness;
	var color = getColor(pleasantness);

	// Outline border
	// ctx.beginPath();
	// ctx.rect(x-outer, y-outer, outer * 2, outer * 2);
	// ctx.lineWidth = 1;
	// ctx.strokeStyle = 'black';
	// ctx.stroke();

	// Outline circle (shows pleasantness)
	ctx.beginPath();
	ctx.arc(x, y, outer - lineWidth, 0, Math.PI * 2);
	ctx.closePath();
	ctx.strokeStyle = 'rgb(' + color.r + ',' + color.g + ',' + color.b + ')';
	ctx.lineWidth = lineWidth
	ctx.stroke();

	// Inner circle (shows eventfulness)
	ctx.beginPath();
	ctx.arc(x, y, inner, 0, Math.PI * 2);
	ctx.closePath();
	ctx.fillStyle = 'rgb(' + color.r + ',' + color.g + ',' + color.b + ')';
	ctx.fill();
};

function createRubenCircle(size, pleasantness, eventfulness)
{
	var canvas = document.createElement('canvas');
	canvas.width = size;
	canvas.height = size;

	var ctx = canvas.getContext('2d');

	drawRubenCircle(ctx, size / 2, size / 2, size / 2, pleasantness, eventfulness);

	return canvas.toDataURL();
}