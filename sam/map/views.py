from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.db.models import Avg
from map.models import Site, Measurement
import json

# Create your views here.
@login_required
def map(request):
	return render_to_response('map.html', context_instance=RequestContext(request))


@login_required
def data_day(request, year, month, day):
	ts_from = "%d-%d-%d" % (int(year), int(month), int(day))
	ts_till = "%d-%d-%d" % (int(year), int(month), int(day) + 1)

	sites = Site.objects.filter(measurements__timestamp__range=[ts_from, ts_till])
	sites = sites.annotate(pleasantness=Avg('measurements__pleasantness'), eventfulness=Avg('measurements__eventfulness'))

	data = [dict(row) for row in sites.values('pk', 'name', 'lat', 'lng', 'pleasantness', 'eventfulness')]

	return HttpResponse(json.dumps(data), content_type='application/json')


@login_required
def data_site(request, site_id):
	site = Site.objects.get(pk=int(site_id))

	measurements = site.measurements.values('timestamp', 'pleasantness', 'eventfulness')

	data = [dict(row) for row in measurements]

	for row in data:
		row['timestamp'] = row['timestamp'].strftime('%Y-%m-%d %H:%M:%S')

	return HttpResponse(json.dumps(data), content_type='application/json')


def test(request):
	return render_to_response('test.html', context_instance=RequestContext(request))
